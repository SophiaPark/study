#1. stash  
---

###git stash save  
현재 작업을 저장해두고 branch를 head로 돌린다.(git reset –hard)  

###git stash list  
저장되어 있는 stash들 보기  

###git stash pop  
stash들은 stack에 저장된다. 따라서 가장 최근에 save한 stash가 현재 branch에 적용된다.  

###git stash apply  
git stash pop 과 비슷한 명령어지만 stash list에서 삭제하지 않는다는 점이 다르다.  

###git stash drop  
필요 없는 stash를 삭제  

###git stash clear  
전체 stash list를 삭제  


#2. commit
---

###git commit --amend  
직전 commit 메세지 수정  
  
###git rebase -i <commit>  
과거 커밋 내용 수정  


#3. add
---

###git reset  
add 취소  
