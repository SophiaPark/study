# -*- coding: utf-8 -*-
import os

# -----------------------------------------
# Ver.1 Recursion
# -----------------------------------------
files = []  # file name List


def search1(dirname):
    global files

    try:
        filenames = os.listdir(dirname)

        for filename in filenames:
            full_filename = os.path.join(dirname, filename)
            if os.path.isdir(full_filename):
                search1(full_filename)  # Recursion Function
            else:
                files.append(filename)
    except Exception as e:
        print e


# -----------------------------------------
# Ver.2 While
# -----------------------------------------
def search2(dirname):
    files = []  # file name List
    subDirStack = []  # sub Directory List

    if os.path.isdir(dirname):
        subDirStack.append(dirname)
    else:
        return

    while subDirStack != []:
        curDir = subDirStack.pop()
        filenames = os.listdir(curDir)

        for filename in filenames:
            full_filename = os.path.join(curDir, filename)
            if os.path.isdir(full_filename):
                subDirStack.append(full_filename)
            else:
                files.append(filename)
    return files


if __name__ == '__main__':
    # Ver.1 Recursion
    search1("/")
    print files

    # Ver.2 While
    files2 = search2("/")
