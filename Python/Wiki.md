#glob
---
파일들의 목록을 뽑을 때 사용  

~~~
>>> import glob
>>> glob.glob('*.*')
['INSTALL.LOG', 'LICENSE.txt', 'NEWS.txt', 'py.ico', 'pyc.ico',
'pycon.ico', 'python.exe', 'pythonw.exe', 'readme.txt',
'UNWISE.EXE', 'w9xpopen.exe']
>>> glob.glob('*.txt')
['LICENSE.txt', 'NEWS.txt', 'readme.txt']
~~~


위의 별표(`*`)는 무슨 글자든, 몇 글자든 상관 없다는 뜻으로,   
디렉토리(폴더)에 들어있는 파일들을 그냥 다 보려면 `*.*` 라고 해주면 된다. 
파일명에 상관 없이 확장자가 txt인 파일을 모두 보고 싶으면 `*.txt` 라고 하면 된다.


출처 : [Python-wikidocs](https://wikidocs.net/83)  


---

#shutil
---
파일 및 디렉토리 작업 모듈  

### copytree  
- src 폴더의 하위 폴더/파일 전부 dst 폴더로 복사  
- 단, 이미 dst 폴더가 존재할 경우 Error 발생  
~~~
>>> import shutil
>>> shutil.copytree([src], [dst])
>>> shutil.copytree(r"C:\Users\sophiaPark\v0001", r"C:\Users\sophiaPark\v0002")  # C:\Users\sophiaPark\v0001를 C:\Users\sophiaPark\에 폴더명을 v0002로 지정하여 생성.
~~~    
  
  
- 기존의 shutil.copytree 사용시 이미 dst 폴더가 존재할 경우 shutil.Error 발생하는 문제 해결  
- it doesn't honor `symlinks` and `ignore` parameters for the root directory of the `src` tree  
- it doesn't raise `shutil.Error` for errors at the root level of `src`  
- in case of errors during copying of a subtree, it will raise `shutil.Error` for that subtree instead of trying to copy other subtrees and raising single combined `shutil.Error`  
- 출처 : [stackoverflow](https://stackoverflow.com/questions/1868714/how-do-i-copy-an-entire-directory-of-files-into-an-existing-directory-using-pyth)

```
#!python
		def copytree(src, dst, symlinks=False, ignore=None):
    		for item in os.listdir(src):
        		s = os.path.join(src, item)
        		d = os.path.join(dst, item)
        		if os.path.isdir(s):
            		shutil.copytree(s, d, symlinks, ignore)
        		else:
            		shutil.copy2(s, d)
```

### copy  
- src 파일을 dst 파일 또는 폴더에 생성(create) 
- 단, 이미 dst 폴더인 경우, src와 동일한 파일명이 존재하는 경우 오버라이트(overwrite)함.
- 단, 이미 dst 폴더인 경우, dst 폴더가 이미 생성되어 있어야 함.

~~~  
>>> import shutil
>>> shutil.copy([src], [dst])
>>> shutil.copy(r"C:\Users\sophiaPark\v0002\test01.txt", r"C:\Users\sophiaPark\v0003")  # C:\Users\sophiaPark\v0002\test01.txt파일을 C:\Users\sophiaPark\v0003에 생성.
~~~

출처 : [Python2-docs](https://docs.python.org/2/library/shutil.html)

---

