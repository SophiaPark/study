** SIGNAL **
===============
---

** [PyQt4 - old style signals slots](http://pyqt.sourceforge.net/Docs/PyQt4/old_style_signals_slots.html)**    
** [PyQt4 - new style signals slots](https://het.as.utexas.edu/HET/Software/PyQt/new_style_signals_slots.html) **  
---

### * `QTreeWidget` 
#### 1. Signal 종류 ( Qt Signals: (quote from PyQt4 QTreeWidget documentation page) )  
- void currentItemChanged (QTreeWidgetItem *,QTreeWidgetItem *)   
- void itemActivated (QTreeWidgetItem *,int)   
- void itemChanged (QTreeWidgetItem *,int)   
- void itemClicked (QTreeWidgetItem *,int)   
- void itemCollapsed (QTreeWidgetItem *)   
- void itemDoubleClicked (QTreeWidgetItem *,int)   
- void itemEntered (QTreeWidgetItem *,int)   
- void itemExpanded (QTreeWidgetItem *)   
- void itemPressed (QTreeWidgetItem *,int)   
- void itemSelectionChanged()    



#### 2. 예제. QTreeWidget 아이템 변경 이벤트 헨들러
##### > old style  
 `self.connect(self.treeWidget, SIGNAL("itemChanged(QTreeWidgetItem *, int)"), [function])`  

##### > new style  
 ` self.treeWidget.itemChanged.connect([function])`  
 
---



