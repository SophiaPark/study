import os
import glob
import shutil
import time
from multiprocessing import Process

dst = "//project/task/v06"

def do_work(src):
    _dst = '%s/%s' % (dst, os.path.basename(src))
    shutil.copy(src, _dst)

def copying_process(src):
    start_time = time.time()
    
    procs = []
    files = []
    
    _src = glob.glob(pathname="%s/*" % src)
    
    # make folder
    if not os.path.isdir(dst):
        os.makedirs(dst)

    # multi processing
    for file in _src:
        if os.path.isfile(file):  # except folder 
            files.append(file)

    for file in files:
        proc = Process(target=do_work, args=(file,))
        procs.append(proc)
        proc.start()

    for proc in procs:
        proc.join()


if __name__ == '__main__':
    src = "//project/task/v02"
    
    copying_process(src)
