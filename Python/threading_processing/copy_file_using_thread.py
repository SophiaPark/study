# -*- coding: utf-8 -*-
import os
import time
import glob
import shutil
import threading


class CopyThread(threading.Thread):
    def __init__(self, srcArr, dst):
        threading.Thread.__init__(self)
        self.srcArr = srcArr
        self.dst = dst

    def run(self):
        for src in self.srcArr:
            shutil.copy(src, self.dst)


# Thread
def copying_thread(devlDir, pubDir):
    plates = glob.glob(pathname="%s/*" % devlDir)  # all files

    # make pub folder
    if not os.path.isdir(pubDir):
        os.makedirs(pubDir)

    # add files
    devlFiles = []
    for plate in plates:
        if os.path.isfile(plate):
            devlFiles.append(plate)

    print "start threading ... "
    start_time = time.time()

    thread = CopyThread(devlFiles, pubDir)
    thread.start()
    thread.join()

    print "Done ... "
    print("--- %s seconds ---" % (time.time() - start_time))


# Thread
def copying_multithread(devlDir, pubDir):
    plates = glob.glob(pathname="%s/*" % devlDir)  # all files

    # make pub folder
    if not os.path.isdir(pubDir):
        os.makedirs(pubDir)

    # add files
    devlFiles = []
    for plate in plates:
        if os.path.isfile(plate):
            devlFiles.append(plate)

    print "start threading ... "
    start_time = time.time()

    t = 5  # Count of Thread 
    for i in range(t):
        if i == t - 1:
            tmp = devlFiles[len(devlFiles) / t * i: len(devlFiles)]
        else:
            tmp = devlFiles[len(devlFiles) / t * i: len(devlFiles) / t * (i + 1)]

        thread = CopyThread(tmp, pubDir)
        thread.start()
        thread.join()

    print "Done ... "
    print("--- %s seconds ---" % (time.time() - start_time))


if __name__ == '__main__':
    devlDir = "//project/task/v01"
    pubDir = "//project/task/v02"
    
    copying_thread(devlDir, pubDir)
    copying_multithread(devlDir, pubDir)
