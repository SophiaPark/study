import os
import glob
import shutil
import time
from multiprocessing import Process

dst = "//project/task/v06"

def do_work(src):
    _dst = '%s/%s' % (dst, os.path.basename(src))
    shutil.copy(src, _dst)

def copying_pool(src):
    start_time = time.time()
    
    procs = []
    files = []
    
    _src = glob.glob(pathname="%s/*" % src)
    
    # make folder
    if not os.path.isdir(dst):
        os.makedirs(dst)

    # multi processing
    for file in _src:
        if os.path.isfile(file):  # except folder 
            files.append(file)

    p = Pool(5)  # Using 5 worker processes
    p.map(do_work, files)


if __name__ == '__main__':
    src = "//project/task/v02"
    
    copying_pool(src)
