"""
Threading - Copy File
- This code will running like multi-threading.
"""
import Queue, threading, os, time
import shutil
import glob

fileQueue = Queue.Queue()
destPath = "//project/task/v01"


class ThreadedCopy:
    totalFiles = 0
    copyCount = 0
    lock = threading.Lock()

    def __init__(self):
        start_time = time.time()
        fileList = []
        src = "//project/task/v02"
        _src = glob.glob(pathname="%s/*" % src)

        # make folder
        if not os.path.isdir(destPath):
            os.makedirs(destPath)

        # multi processing
        for file in _src:
                if os.path.isfile(file):  # except folder
                    fileList.append(file)

        self.totalFiles = len(fileList)

        print str(self.totalFiles) + " files to copy."
        self.threadWorkerCopy(fileList)

        print("--- %s seconds ---" % (time.time() - start_time))

    def CopyWorker(self):
        while True:
            fileName = fileQueue.get()
            shutil.copy(fileName, destPath)
            fileQueue.task_done()
            with self.lock:
                self.copyCount += 1
                percent = (self.copyCount * 100) / self.totalFiles
                print str(percent) + " percent copied."

    def threadWorkerCopy(self, fileNameList):
        for i in range(16):
            t = threading.Thread(target=self.CopyWorker)
            t.daemon = True
            t.start()
        for fileName in fileNameList:
            fileQueue.put(fileName)
        fileQueue.join()


ThreadedCopy()
