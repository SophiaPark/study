MultiProcessing & MultiTreading
===
---

일반적으로는 멀티 쓰레딩이 멀티 프로세싱 보다 효율적이다.   
멀티 프로세싱을 위해서 프로세스를 더 생성하게 되면 생성되는 프로세스는 기존에 프로세스가 가지는 데이터와 공간을 똑같이 복사하여 가지게 된다.  
반면에 쓰레드는 프로세스의 공간 및 자원을 다른 쓰레드와 공유하고 스택만을 따로 가지기때문에 생성되는 시간이 짧으며, 컨텍스트 스위칭이 일어날 때도 프로세스에 비해 오버헤드가 적게 발생한다.  

하지만 `파이썬`에서는 `GIL(Global Interpreter Lock)`으로 인해 `멀티 쓰레딩을 하더라도 싱글 쓰레드보다 속도면에서 이점을 가지기 힘들다`.   
GIL은 자원의 보호를 위해 파이썬 프로그램이 실행될 때 여러개의 쓰레드가 실행되더라도 `한번에 하나의 쓰레드만 실행`되도록 한다.  

이 때문에 파이썬에서 연산부분을 멀티 쓰레딩으로 구현해도 성능 향상을 기대할 수 없다. (I/O, 비동기에서는 활용 가능)    
단, GIL은 유저 레벨 쓰레드에만 영향을 미치므로 I/O 등 커널 레벨에서 실행되는 작업의 경우에는 GIL의 영향을 받지 않는다.  

출처 : [Python Multi-Threading, Multi-Processing](https://lee-seul.github.io/python/2017/05/02/python-gil.html)
  
> 

---  

예제 파일)  

파일 복사는 물리적인 작업이기 때문에 속도 차이가 크지 않다. ( 많아야 1.1배쯤? )  
파일 IO는 필연적으로 락을 걸고 동기해줘야하기 때문에 멀티스레딩을 한다고 해서 더 빨라지지 않는다고 한다. [[참고]](https://kldp.org/node/137057)  
그래서, 아래 파일을 통해서는 속도차이를 크게 느낄수 없으니 Threading, Processing 사용 방법정도만 확인하면 된다.  

- threading / multitreading 을 이용한 파일 복사 : copy_file_using_thread.py  
- theading 을 이용한 파일 복사 ( multi-threading 처럼 작동됨 ) : copy_file_using_thread2.py
- multi-processing 을 이용한 파일 복사 :   
--- copy_file_using_processing.py : Process 이용  
--- copy_file_using_pool.py       : Pool 이용


